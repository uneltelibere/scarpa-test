package eu.uneltelibere.scarpa.test;

public class PostgresContainerRule extends DockerContainerRule {

	private static final int PORT = 5432;

	public PostgresContainerRule(final String image) {
		super(image, PORT);
	}

	@Override
	protected void before() throws Throwable {
		super.before();

		if (mappedPort() > 0) {
			System.setProperty("dbPort", String.valueOf(mappedPort()));
		}
	}
}
