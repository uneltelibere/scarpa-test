package eu.uneltelibere.scarpa.test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Map;

import org.junit.rules.ExternalResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.NetworkSettings;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.core.DockerClientBuilder;

public class DockerContainerRule extends ExternalResource {

	private static final Logger LOG = LoggerFactory.getLogger(DockerContainerRule.class);

	private final DockerClient dockerClient;

	private final String image;
	private boolean containerWasAlreadyRunning;
	private String containerId;
	private int portToExpose;
	private int mappedPort;

	public DockerContainerRule(String image, int portToExpose) {
		dockerClient = DockerClientBuilder.getInstance().build();
		this.image = image;
		this.portToExpose = portToExpose;
	}

	@Override
	protected void before() throws Throwable {
		super.before();

		List<Container> containers = dockerClient.listContainersCmd().exec();
		LOG.debug("Found {} running containers", containers.size());
		for (Container container : containers) {
			String containerImage = container.getImage();
			LOG.debug("running container: {}", containerImage);

			String latestContainer = containerImage;
			if (image.endsWith("latest") && !containerImage.contains(":")) {
				latestContainer += ":latest";
			}

			if (image.equals(latestContainer)) {
				LOG.info("Expected container {} is already running, no need to start it again.", image);
				containerWasAlreadyRunning = true;

				containerId = container.getId();
				ExposedPort tcpPort = ExposedPort.tcp(portToExpose);
				waitForPort(tcpPort);
			}
		}
		if (!containerWasAlreadyRunning) {
			LOG.info("Expected container {} is not already running, need to start it.", image);
			startContainer();
		}
	}

	private void startContainer() {
		ExposedPort tcpPort = ExposedPort.tcp(portToExpose);

		CreateContainerResponse container = dockerClient.createContainerCmd(image).withExposedPorts(tcpPort)
				.withPublishAllPorts(true).exec();
		containerId = container.getId();
		LOG.info("Created container {} from image {}.", containerId, image);

		dockerClient.startContainerCmd(containerId).exec();
		waitForPort(tcpPort);
	}

	private void waitForPort(ExposedPort tcp) {
		InspectContainerResponse inspectedContainer = dockerClient.inspectContainerCmd(containerId).exec();
		NetworkSettings ns = inspectedContainer.getNetworkSettings();
		Ports ports = ns.getPorts();
		Map<ExposedPort, Ports.Binding[]> map = ports.getBindings();
		Ports.Binding[] bindings = map.get(tcp);

		mappedPort = Integer.parseInt(bindings[0].getHostPortSpec());

		LOG.info("Port {} is mapped to {}.", tcp.getPort(), mappedPort);
		String host = ns.getNetworks().get("bridge").getIpAddress();
		waitForPort(host, portToExpose, 20_000);
	}

	private void waitForPort(String host, int port, long timeoutInMillis) {
		LOG.debug("Waiting for port {} to be available on {}.", port, host);
		SocketAddress address = new InetSocketAddress(host, port);
		long totalWait = 0;
		while (true) {
			try {
				SocketChannel.open(address);
				LOG.debug("Port {} on {} was available after {} ms.", port, host, totalWait);
				return;
			} catch (IOException e) {
				try {
					Thread.sleep(100);
					totalWait += 100;
					if (totalWait > timeoutInMillis) {
						throw new IllegalStateException("Timeout while waiting for port " + port + " on host " + host);
					}
				} catch (InterruptedException ie) {
					LOG.warn("Interrupted", ie);
					Thread.currentThread().interrupt();
				}
			}
		}
	}

	@Override
	protected void after() {
		super.after();
		if (containerWasAlreadyRunning) {
			LOG.info("Expected container {} was already running, no need to stop it.", image);
		} else {
			LOG.info("Expected container {} was not already running, and was created as {}. Need to stop it.", image,
					containerId);
			dockerClient.stopContainerCmd(containerId).withTimeout(2).exec();
			LOG.debug("Container stopped.");
			dockerClient.removeContainerCmd(containerId).exec();
			LOG.debug("Container removed.");
		}
	}

	protected int mappedPort() {
		return mappedPort;
	}
}
