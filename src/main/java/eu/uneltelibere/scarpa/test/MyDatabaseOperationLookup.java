package eu.uneltelibere.scarpa.test;

import java.util.HashMap;
import java.util.Map;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.operation.DefaultDatabaseOperationLookup;

public class MyDatabaseOperationLookup extends DefaultDatabaseOperationLookup {

	private static final Map<DatabaseOperation, org.dbunit.operation.DatabaseOperation> MY_LOOKUP = new HashMap<DatabaseOperation, org.dbunit.operation.DatabaseOperation>();

	static {
		MY_LOOKUP.put(DatabaseOperation.CLEAN_INSERT,
				new ResetSequenceOperationDecorator(org.dbunit.operation.DatabaseOperation.CLEAN_INSERT));
	}

	@Override
	public org.dbunit.operation.DatabaseOperation get(DatabaseOperation operation) {
		if (MY_LOOKUP.containsKey(operation)) {
			return MY_LOOKUP.get(operation);
		}
		return super.get(operation);
	}
}