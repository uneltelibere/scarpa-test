package eu.uneltelibere.scarpa.test;

import java.sql.SQLException;
import java.sql.Statement;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResetSequenceOperationDecorator extends DatabaseOperation {

	private static final Logger LOG = LoggerFactory.getLogger(ResetSequenceOperationDecorator.class);

	private DatabaseOperation databaseOperation;

	public ResetSequenceOperationDecorator(DatabaseOperation databaseOperation) {
		this.databaseOperation = databaseOperation;
	}

	@Override
	public void execute(IDatabaseConnection connection, IDataSet dataSet) throws DatabaseUnitException, SQLException {
		String[] tables = dataSet.getTableNames();
		try (Statement statement = connection.getConnection().createStatement()) {
			for (String table : tables) {
				try {
					int startWith = dataSet.getTable(table).getRowCount() + 1;
					statement.execute("ALTER SEQUENCE " + table + "_id_seq RESTART WITH " + startWith);
				} catch (SQLException ex) {
					LOG.warn("Can't reset sequence {}.", table, ex);
				}
			}
		}
		databaseOperation.execute(connection, dataSet);
	}
}
